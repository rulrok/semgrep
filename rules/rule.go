package rules

// SemgrepRuleFile represents the structure of a Semgrep rule YAML file.
type SemgrepRuleFile struct {
	Rules []SemgrepRule `yaml:"rules"`
}

// SemgrepRule is an abridged representation of a single Semgrep rule within
// a YAML file. The `,inline` flag is used to collect the properties of the rule
// we're not interested in. See https://pkg.go.dev/gopkg.in/yaml.v3#Marshal
// for more information.
type SemgrepRule struct {
	ID       string `yaml:"id"`
	Metadata struct {
		PrimaryIdentifier    string `yaml:"primary_identifier"`
		SecondaryIdentifiers []struct {
			Name  string `yaml:"name"`
			Type  string `yaml:"type"`
			Value string `yaml:"value"`
		} `yaml:"secondary_identifiers"`
	} `yaml:"metadata"`
	Rest map[string]interface{} `yaml:",inline"`
}
