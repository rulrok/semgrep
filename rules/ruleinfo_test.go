package rules

import (
	"bytes"
	"path"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
)

func TestBuildRuleMapWithFullPath(t *testing.T) {
	expect := map[string]map[string]bool{
		"../testdata/sampledist/bandit.yml": {
			"bandit.B303-1": true,
			"bandit.B101-1": true,
		},
		"../testdata/sampledist/eslint.yml": {
			"eslint.detect-no-csrf-before-method-override-1": true,
			"eslint.detect-non-literal-require-1":            true,
		},
		"../testdata/sampledist/find_sec_bugs.yml": {
			"find_sec_bugs.DMI_EMPTY_DB_PASSWORD-1.HARD_CODE_PASSWORD-2": true,
		},
		"../testdata/sampledist/flawfinder.yml": {
			"flawfinder.char-1.TCHAR-1.wchar_t-1": true,
		},
		"../testdata/sampledist/gosec.yml": {
			"gosec.G107-1": true,
		},
		"../testdata/sampledist/security_code_scan.yml": {
			"security_code_scan.SCS0005-1":           true,
			"security_code_scan.SCS0026-1.SCS0031-1": true,
		},
	}

	defaultConfigPath := path.Join("../testdata", "sampledist")
	ruleMapFullPath, err := BuildRuleMap(defaultConfigPath, true)
	require.NoError(t, err)

	for expectedFilename, expectedRuleIds := range expect {
		require.Contains(t, ruleMapFullPath, expectedFilename)
		rule := ruleMapFullPath[expectedFilename]
		for _, r := range rule.Rules {
			require.Contains(t, expectedRuleIds, r.ID)
		}
	}
}

func TestBuildRuleMap(t *testing.T) {
	expect := map[string]map[string]bool{
		"bandit": {
			"bandit.B303-1": true,
			"bandit.B101-1": true,
		},
		"eslint": {
			"eslint.detect-no-csrf-before-method-override-1": true,
			"eslint.detect-non-literal-require-1":            true,
		},
		"find_sec_bugs": {
			"find_sec_bugs.DMI_EMPTY_DB_PASSWORD-1.HARD_CODE_PASSWORD-2": true,
		},
		"flawfinder": {
			"flawfinder.char-1.TCHAR-1.wchar_t-1": true,
		},
		"gosec": {
			"gosec.G107-1": true,
		},
		"security_code_scan": {
			"security_code_scan.SCS0005-1":           true,
			"security_code_scan.SCS0026-1.SCS0031-1": true,
		},
	}

	defaultConfigPath := path.Join("../testdata", "sampledist")
	ruleMapFullPath, err := BuildRuleMap(defaultConfigPath, false)
	require.NoError(t, err)

	for expectedFilename, expectedRuleIds := range expect {
		require.Contains(t, ruleMapFullPath, expectedFilename)
		rule := ruleMapFullPath[expectedFilename]
		for _, r := range rule.Rules {
			require.Contains(t, expectedRuleIds, r.ID)
		}
	}
}

func TestComputeRuleStats(t *testing.T) {
	var buffer bytes.Buffer
	dummyLogger := logrus.New()
	dummyLogger.SetFormatter(&logrus.TextFormatter{
		DisableColors:    true,
		DisableTimestamp: true,
	})

	dummyLogger.SetOutput(&buffer)

	stats := NewAnalysisStats(dummyLogger, "../testdata/rules-without-clash")
	err := stats.Compute()
	require.NoError(t, err)

	require.Equal(t, 2, stats.numActiveRuleFiles())
	require.Equal(t, 3, stats.numActiveRuleIDs())
	require.Equal(t, 0, stats.numRuleIDClashes())
	require.Equal(t, "d540cd83eeec29479dc1c45e3309e0661b7a3f0f43e56ffea1c2830216406b49", stats.combinedChecksum)

	stats.LogStatus()
	require.Equal(t, `level=info msg="2 active rule files detected with 3 active rules"
level=info msg=" * rule file '../testdata/rules-without-clash/rule-LDAPInjection.yml': 'efdc5ad932fe08a59cbf750a52004f83fce23b7fbb4ef598c10e013c8d2fdf51'"
level=info msg=" * rule file '../testdata/rules-without-clash/rule-concat-sqli.yml': '5027676311a38a90077875f78b1a5e7c95beda527567f56a36e8aa5835424a28'"
level=info msg="Combined rule checksum: 'd540cd83eeec29479dc1c45e3309e0661b7a3f0f43e56ffea1c2830216406b49'"
`, buffer.String())

	buffer.Reset()

	stats = NewAnalysisStats(dummyLogger, "../testdata/rules-with-clash")
	err = stats.Compute()

	require.NoError(t, err)
	require.Equal(t, 2, stats.numActiveRuleFiles())
	require.Equal(t, 3, stats.numActiveRuleIDs())
	require.Equal(t, 1, stats.numRuleIDClashes())
	require.Equal(t, "0cb99f964edac82663f165eb1471044ca5889393879d0665a5d16bef42dd4a84", stats.combinedChecksum)

	stats.LogStatus()
	require.Equal(t, `level=warning msg="Rule ID java_inject_rule-MyLDAPInjection is used 2 times"
level=info msg="2 active rule files detected with 3 active rules"
level=info msg=" * rule file '../testdata/rules-with-clash/rule-LDAPInjection.yml': '099e2967d3fb5015f17fae5e46b47c44e5571bfe3c9302d890cde6248e9d4934'"
level=info msg=" * rule file '../testdata/rules-with-clash/rule-concat-sqli.yml': '5027676311a38a90077875f78b1a5e7c95beda527567f56a36e8aa5835424a28'"
level=info msg="Combined rule checksum: '0cb99f964edac82663f165eb1471044ca5889393879d0665a5d16bef42dd4a84'"
`, buffer.String())
}
