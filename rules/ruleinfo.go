package rules

import (
	"crypto/sha256"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path"
	"path/filepath"
	"sort"

	"gopkg.in/yaml.v2"

	"strings"

	log "github.com/sirupsen/logrus"
)

// Compute some rule statistics and meta-information that is
// made available through the AnalysisStats object
func (stats *AnalysisStats) Compute() error {
	ruleMap, err := BuildRuleMap(stats.rulepath, true)
	if err != nil {
		return err
	}

	var combinedString strings.Builder

	// identify clashing rule IDs
	// build list of rule IDs for searching
	// build list of rule files
	for ruleFile, rule := range ruleMap {
		stats.ruleFiles = append(stats.ruleFiles, ruleFile)
		for _, r := range rule.Rules {
			stats.ruleIDs[r.ID]++
		}
	}

	sort.Strings(stats.ruleFiles)

	// compute checksums for each rule file
	// compute checksum for all rule files
	for _, ruleFile := range stats.ruleFiles {
		checksumString, err := sha256FromFile(ruleFile)
		if err != nil {
			return err
		}

		stats.ruleFileChecksums[ruleFile] = checksumString
		combinedString.WriteString(checksumString)
		combinedString.WriteRune('\n')
	}

	combinedChecksum := sha256.Sum256([]byte(combinedString.String()))
	stats.combinedChecksum = fmt.Sprintf("%x", combinedChecksum)
	return nil
}

// AnalysisStats includes meta-information about semgrep rules
type AnalysisStats struct {
	combinedChecksum  string
	ruleFileChecksums map[string]string
	ruleIDs           map[string]int
	ruleFiles         []string
	rulepath          string
	logger            *log.Logger
}

// NewAnalysisStats generates a new stats object
func NewAnalysisStats(logger *log.Logger, rulepath string) AnalysisStats {
	return AnalysisStats{
		combinedChecksum:  "",
		ruleFileChecksums: map[string]string{},
		ruleIDs:           map[string]int{},
		ruleFiles:         []string{},
		logger:            logger,
		rulepath:          rulepath,
	}
}

// LogStatus logs meta-information about the analysis
func (stats AnalysisStats) LogStatus() {
	if len(stats.ruleFileChecksums) == 0 {
		stats.logger.Errorf("No active rules detected")
		return
	}

	for id, count := range stats.ruleIDs {
		if count > 1 {
			stats.logger.Warnf("Rule ID %s is used %d times", id, count)
		}
	}
	stats.logger.Infof("%d active rule files detected with %d active rules", len(stats.ruleFileChecksums), len(stats.ruleIDs))

	for _, file := range stats.ruleFiles {
		if _, ok := stats.ruleFileChecksums[file]; !ok {
			continue
		}
		stats.logger.Infof(" * rule file '%s': '%s'", file, stats.ruleFileChecksums[file])
	}
	stats.logger.Infof("Combined rule checksum: '%s'", stats.combinedChecksum)
}

func (stats AnalysisStats) numActiveRuleFiles() int {
	return len(stats.ruleFileChecksums)
}

func (stats AnalysisStats) numActiveRuleIDs() int {
	return len(stats.ruleIDs)
}

func (stats AnalysisStats) numRuleIDClashes() int {
	c := 0
	for _, count := range stats.ruleIDs {
		if count > 1 {
			c++
		}
	}
	return c
}

func sha256FromFile(filepath string) (string, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return "", err
	}
	defer file.Close()

	hash := sha256.New()
	if _, err = io.Copy(hash, file); err != nil {
		return "", err
	}
	checksum := hash.Sum(nil)
	return fmt.Sprintf("%x", string(checksum)), nil
}

// BuildRuleMap creates a map that includes filenames and their mapping to the
// corresponding semgrep rule file object
func BuildRuleMap(configPath string, useFullPath bool) (map[string]SemgrepRuleFile, error) {
	ruleMap := map[string]SemgrepRuleFile{}

	err := filepath.WalkDir(configPath, func(p string, d fs.DirEntry, err error) error {
		if _, err = os.Stat(p); err != nil || d.IsDir() {
			return nil
		}

		ext := path.Ext(p)
		if ext != ".yml" && ext != ".yaml" {
			log.Debugf("skipping parse for non-rule file: %s", p)
			return nil
		}

		var ruleFile SemgrepRuleFile

		fileContent, err := os.ReadFile(p)
		if err != nil {
			return fmt.Errorf("read rule file at %s: %w", p, err)
		}

		if err = yaml.Unmarshal(fileContent, &ruleFile); err != nil {
			return fmt.Errorf("parse rule file at %s: %w", p, err)
		}

		if useFullPath {
			ruleMap[p] = ruleFile
		} else {
			rulesetFile := strings.Split(filepath.Base(p), ".")[0]
			ruleMap[rulesetFile] = ruleFile
		}

		return nil
	})

	return ruleMap, err
}
