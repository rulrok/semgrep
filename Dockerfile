# When updating version make sure to check on semgrepignore file as well
ARG SCANNER_VERSION=1.72.0
ARG POST_ANALYZER_SCRIPTS_VERSION=0.4.0
ARG TRACKING_CALCULATOR_VERSION=2.4.1

FROM registry.gitlab.com/security-products/post-analyzers/scripts:${POST_ANALYZER_SCRIPTS_VERSION} AS scripts
FROM registry.gitlab.com/security-products/post-analyzers/tracking-calculator:${TRACKING_CALCULATOR_VERSION} AS tracking

FROM golang:1.22.3-alpine AS build

ENV CGO_ENABLED=0 GOOS=linux

WORKDIR /go/src/buildapp
COPY . .

# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=`go list -m` && \
        go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer-semgrep

FROM python:3.9-alpine

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION}
ENV SEMGREP_R2C_INTERNAL_EXPLICIT_SEMGREPIGNORE "/semgrepignore"
ENV PIP_NO_CACHE_DIR=off
ENV SAST_RULES_PROJECTID=27038823
ENV SAST_RULES_VERSION=2.5.5

RUN mkdir -p /etc/ssl/certs/ && \
    touch /etc/ssl/certs/ca-certificates.crt && \
    chmod g+w /etc/ssl/certs/ca-certificates.crt

COPY --from=build /analyzer-semgrep /analyzer-binary
COPY semgrepignore /semgrepignore
RUN mkdir /.cache && \
    chmod -R g+rw /.cache && \
    \
    # Configure CA Certificates
    mkdir -p /etc/ssl/certs/ && \
    touch /etc/ssl/certs/ca-certificates.crt && \
    chmod g+w /etc/ssl/certs/ca-certificates.crt && \
    \
    # Install semgrep and additional tools
    apk add --no-cache wget zip git && \
    apk add --no-cache --virtual=.build-only-deps gcc musl-dev && \
    pip install semgrep==$SCANNER_VERSION && \
    apk del .build-only-deps

RUN wget https://gitlab.com/api/v4/projects/${SAST_RULES_PROJECTID}/packages/generic/sast-rules/v${SAST_RULES_VERSION}/sast-rules.zip && \
    unzip sast-rules.zip && \
    rm sast-rules.zip && \
    mv dist/ /rules

RUN apk del wget zip

COPY --from=tracking /analyzer-tracking /analyzer-tracking
COPY --from=scripts /start.sh /analyzer

# Allow running on m1s in Docker
RUN sed -i 's/if platform.machine() in {"arm64", "aarch64"} and platform.system() == "Linux":/if 0:/g' /usr/local/lib/python3.9/site-packages/semgrep/cli.py

ENTRYPOINT []
CMD \
    echo "Using rules from https://gitlab.com/gitlab-org/security-products/sast-rules/-/tree/v${SAST_RULES_VERSION}" ; \
    /analyzer run